### [Все таблицы](https://some.link)



## Таблица: users_fingerprint



### Схема: shema

Данные, которые передаются GA в момент старта сессии





|**Field**|**Type**|**Description**

|---|---|---

|user_id|int|айди пользователя

|dt|timestamp|дата старта сессии

|fingerprint|varchar(255)|

|accept_header|varchar(255)|

|encoding_header|varchar(255)|

|language_header|varchar(255)|

|donottrack|varchar(40)|

|time_zone|varchar(40)|

|screen_resolution|varchar(40)|расширение экрана

|local_storage|int|

|session_storage|int|

|adblock|int|
